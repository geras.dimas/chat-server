#!/usr/bin/env node
try {
    global.__base = __dirname + '/';

    /**
     * Module dependencies.
     */
    let http = require("http");
    let io = require('socket.io');
    let app = require("./app");
    let config = require("./src/config.js");
    let log = require("./src/log");
    let websocket = require("./src/ws/websocket.js");
    let subscribeService = require("./src/services/subscribeService.js")
    /**
     * Get port from config and store in Express.
     */
    let port = normalizePort(config.get("port") || "3000");
    app.set("port", port);

    /**
     * Create HTTP server.
     */
    let server = http.createServer(app);

    /**
     * Create Web Sockets server.
     */

    let wsServer = io(server);
    subscribeService.init(wsServer);
    websocket.init(wsServer);


    /**
     * Listen on provided port, on all network interfaces.
     */
    server.listen(port);
    server.on("error", onError);
    server.on("listening", onListening);

    /**
     * Normalize a port into a number, string, or false.
     */
    function normalizePort(val) {
        let port = parseInt(val, 10);

        if (isNaN(port)) {
            // named pipe
            return val;
        }

        if (port >= 0) {
            // port number
            return port;
        }

        return false;
    }

    /**
     * Event listener for HTTP server "error" event.
     */
    function onError(error) {
        log.info("Express server was unable to start", error);

        if (error.syscall !== "listen") {
            throw error;
        }

        let bind = typeof port === "string"
            ? "Pipe " + port
            : "Port " + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case "EACCES":
                console.error(bind + " requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(bind + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */
    function onListening() {
        let addr = server.address();
        let bind = typeof addr === "string"
            ? "pipe " + addr
            : "port " + addr.port;
        log.info("Listening on " + bind);
        log.info("Server listening on port " + port);
    }

} catch (err) {
    console.log(err);
    throw err;
}
