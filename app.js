let express = require("express");
let path = require("path");
let bodyParser = require("body-parser");
let log = require(__base + "src/log");
require("./src/data/mongoose"); // todo do not remove this import;

let auth = require("./src/api/auth");

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.static(path.join(__dirname, "resources")));
app.use(auth);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error("Not Found");
    err.status = 404;
    log.debug("Not Found 404 error", req.originalUrl);
    next(err);
});

app.use(function (err, req, res, next) {
    if (err.status !== 404) {
        log.error(err);
    }
    res
        .status(err.status || 500)
        .send({error: req.app.get("env") === "development" ? err : "Internal Server Error"});
});

module.exports = app;
