let express = require("express");
let router = express.Router();
let AuthService = require("../services/authService");

/* Login into system. There can be several ways to login.

1. By telephone number and firebase token
{
    firebaseToken: "123456" // code
}
2. By refreshToken (token of type 2)
{
    refreshToken: "your token"
}
3. Check token validity
{
    token: "your token"
}
 */

router.post("/login", function (req, res, next) {
     /* if (req.body.firebaseToken) {
        AuthService.loginByFirebaseToken(req.body.firebaseToken)
          .then(function (authInfo) {
            res.send(authInfo);
          }, function (err) {
            if (typeof err === 'string' || err instanceof String) {
              res.statusCode = 400;
              res.send({error: err});
            } else {
              res.statusCode = 500;
              res.send({error: "Internal Server Error"});
            }
          });
      } else if (req.body.refreshToken) {
        // return new pair of tokens and modify user's refresh token salt
        AuthService.refreshToken(req.body.refreshToken)
          .then(function (authInfo) {
            res.send(authInfo);
          }, function (err) {
            if (typeof err === 'string' || err instanceof String) {
              res.statusCode = 400;
              res.send({error: err});
            } else {
              res.statusCode = 500;
              res.send({error: "Internal Server Error"});
            }
          });
      } else if (req.body.token) {
        AuthService.checkTokenAndUser(req.body.token)
          .then(function () {
            res.send('');
          }, function (err) {
            if (typeof err === 'string' || err instanceof String) {
              res.statusCode = 400;
              res.send({error: err});
            } else {
              res.statusCode = 500;
              res.send({error: "Internal Server Error"});
            }
          });
      }*/
     AuthService.test(req.body.number).then(function (authInfo) {
        res.send(authInfo);
    }, function (err) {
        if (typeof err === 'string' || err instanceof String) {
            res.statusCode = 400;
            res.send({error: err});
        } else {
            res.statusCode = 500;
            res.send({error: "Internal Server Error"});
        }
    });




});

module.exports = router;
