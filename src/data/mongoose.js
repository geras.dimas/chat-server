let mongoose = require("mongoose");
let config = require(__base + "src/config.js");
let log = require(__base + "src/log.js");

mongoose.Promise = global.Promise; // force Mongoose to use global native ES6 promises
mongoose.connect(config.get("mongoose:uri"), {
    useMongoClient: true
});
let db = mongoose.connection;

db.on("error", function (err) {
    log.error("DB connection error: " + err.message);
});

db.once("open", function callback() {
    log.info("Connected to Mongo DB at url: " + config.get("mongoose:uri"));
});

module.exports = db;
