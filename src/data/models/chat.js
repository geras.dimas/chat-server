let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let uuid = require("uuid");

// Чат
let Chat = new Schema({
    _id: {type: String, default: uuid.v4}, // guid
    header: {type: String},
    avatar: {type: String, ref:"File"}, // guid
    owner: {type: String, required: true, ref: 'User'},
    type: {type: Number},
    private: {type: Boolean, default: false},
    members: [{type: String, required: true, ref: 'User'}] // guids
}, {
    timestamps: true
});

let ChatModel = mongoose.model("Chat", Chat);

module.exports.ChatModel = ChatModel;
