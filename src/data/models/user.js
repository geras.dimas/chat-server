let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let uuid = require("uuid");

// Пользователь
let User = new Schema({
    _id: {type: String, default: uuid.v4}, // guid
    tel: {type: String, required: true},
    name: {type: String},
    avatar: {type: String, ref: "File"}// guid
}, {
    timestamps: true
});

let UserModel = mongoose.model("User", User);

module.exports.UserModel = UserModel;
