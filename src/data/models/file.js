let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let uuid = require("uuid");

let File = new Schema({
  _id: {type: String, default: uuid.v4}, // guid
  fileName: {type: String},
  type: {type: String},
  size: {type: Number}
}, {
  timestamps: true
});

let fileModel = mongoose.model("File", File);

module.exports.FileModel = fileModel;
