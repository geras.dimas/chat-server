let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let uuid = require("uuid");

// Чат
let ChatMessage = new Schema({
    _id: {type: String, default: uuid.v4}, // guid
    message: {type: String, required: true, maxlength: 8192},
    type: {type: Number, required: true},
    author: {type: String, required: true, ref: 'User'}, // guid
    chatId: {type: String, required: true, ref: 'Chat'},
    file:{type:String, ref: 'File'}
    // guid
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

let ChatMessageModel = mongoose.model("ChatMessage", ChatMessage);

module.exports.ChatMessageModel = ChatMessageModel;
