let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let uuid = require("uuid");
let config = require(__base + "src/config.js");

let authConfig = config.get("auth:refreshTokenExpires");

// Пользователь
let UsedRefreshToken = new Schema({
  _id: {type: String, default: uuid.v4}, // guid
  createdAt: {type: Date, expires: authConfig.refreshTokenExpires, default: Date.now, required: true}
  // the record will be automatically deleted by mongo after expires period
  // https://docs.mongodb.com/manual/tutorial/expire-data/
});

let UsedRefreshTokenModel = mongoose.model("UsedRefreshToken", UsedRefreshToken);

module.exports.UsedRefreshTokenModel = UsedRefreshTokenModel;
