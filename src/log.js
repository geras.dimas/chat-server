let winston = require("winston");
let config = require(__base + "src/config.js");

let loggerConfig = config.get("logger");
let transports = [];
if (loggerConfig) {
    for (let transportName in loggerConfig) {
        if (winston.transports[transportName]) {
            transports.push(new (winston.transports[transportName])(loggerConfig[transportName]));
        } else {
            console.error("Transport " + transportName + " is not available in Winston transport list");
        }
    }
} else {
    console.error("There is no configured transports for Winston log engine!!! By default, only the Console transport is set");
}

winston.configure({transports: transports});

// Levels of logging
// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }

module.exports = winston;
