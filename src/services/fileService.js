let fs = require('fs');
let log = require(__base + "src/log");
let config = require(__base + "src/config");
let filesFolder = config.get("files:local:folder");
let avatarFolder = config.get("files:avatar:folder");
/*let fileMaxSize = config.get("files:maxSize");
let avatarMaxSize = config.get("files:avatar:maxSize");
let avatarType = config.get("files:avatar:type");
*/
let FileModel = require(__base + "src/data/models/file").FileModel;
let azure = require('azure-storage');
let BufferStream = require('azure-storage/lib/common/streams/bufferstream');
let FileStream = require('azure-storage/lib/common/streams/filereadstream');
let azureStorageConnectionString = config.get("files:azure:azureStorageConnectionString");
let azureStorageContainer = config.get("files:azure:azureStorageContainer");
let azureAvatarStorageContainer = config.get("files:azure:azureAvatarStorageContainer");

function saveFileAzure_old(dataArray, fileName, uuid, isAvatar) {
    let blobSvc = azure.createBlobService(azureStorageConnectionString);
    let buffer = new Buffer(dataArray);
    let stream = new BufferStream(buffer);
    let lenght = buffer.length * 2;
    return new Promise(function (resolve, reject) {
        let storageContainer = azureStorageContainer;
        if (isAvatar) {
            storageContainer = azureAvatarStorageContainer
        }
        blobSvc.createContainerIfNotExists(storageContainer, function (error, result, response) {
            if (error) {
                log.error(error);
                reject("Can not save file");
            } else {
                blobSvc.createBlockBlobFromStream(storageContainer, uuid, stream, lenght, function (error, result, response) {
                    if (error) {
                        log.error(error);
                        reject("Can not save file");
                    } else {
                        resolve(uuid);
                    }
                });
            }
        });
    });
}

function saveFileAzure( size, uuid, isAvatar) {
    let blobSvc = azure.createBlobService(azureStorageConnectionString);
    let fileDir = filesFolder;
    if (!fs.existsSync(filesFolder)) {
        fs.mkdirSync(filesFolder);
    }
    if (isAvatar) {
        fileDir += avatarFolder;
        if (!fs.existsSync(fileDir)) {
            fs.mkdirSync(fileDir);
        }
    }
    let stream = new FileStream(fileDir + "/" + uuid);
    let lenght = size * 2;
    return new Promise(function (resolve, reject) {
        let storageContainer = azureStorageContainer;
        if (isAvatar) {
            storageContainer = azureAvatarStorageContainer
        }
        blobSvc.createContainerIfNotExists(storageContainer, function (error, result, response) {
            if (error) {
                log.error(error);
                reject("Can not save file");
            } else {
                blobSvc.createBlockBlobFromStream(storageContainer, uuid, stream, lenght, function (error, result, response) {
                    if (error) {
                        log.error(error);
                        reject("Can not save file");
                    } else {
                        resolve(uuid);
                    }
                });
            }
        });
    });
}
function removeFileAzure(uuid, isAvatar) {
    return new Promise(function (resolve, reject) {
        let blobSvc = azure.createBlobService(azureStorageConnectionString);
        let storageContainer = azureStorageContainer;
        if (isAvatar) {
            storageContainer = azureAvatarStorageContainer
        }
        blobSvc.deleteBlob(storageContainer, uuid, function (error, response) {
            if (error) {
                if (error.statusCode == 404) {
                    resolve({uuid: uuid});
                } else {
                    log.error(error);
                    reject("Can not delete file");
                }
            } else {
                resolve({uuid: uuid});
            }
        });
    });
}

function newFile(fileName, type, size, uuid) {
    return FileModel
        .create({_id: uuid, fileName: fileName, type: type, size: size});
}

function removeFile(uuid) {
    return FileModel
        .findByIdAndRemove(uuid).exec();
}

function deleteFile(uuid, isAvatar) {
    return new Promise(function (resolve, reject) {
        removeFileAzure(uuid, isAvatar)
            .then(function (res) {
                    removeFile(uuid)
                        .then(function (result) {
                            resolve({uuid: uuid, state: "delete"});
                        }, function () {
                            reject("Can not delete file");
                        })
                },
                function () {
                    reject("Can not delete file");
                });
    });
}

function getFile(uuid) {
    return FileModel.findById(uuid).exec();
}

function saveFile_old(dataArray, fileName, type, uuid, isAvatar) {
    return new Promise(function (resolve, reject) {
        saveFileAzure(dataArray, fileName, uuid, isAvatar)
            .then(function (uuid) {
                newFile(fileName, type, dataArray.length, uuid)
                    .then(function (file) {
                        resolve(file);
                    }, function () {
                        removeFileAzure(uuid, isAvatar)
                        reject("Can not save file")
                        return
                    });
            }, function (error) {
                reject("Can not save file");
                return;
            })
    });
}

function saveFile(size, fileName, type, uuid, isAvatar) {
    return new Promise(function (resolve, reject) {
        saveFileAzure(size, uuid, isAvatar)
            .then(function (uuid) {
                newFile(fileName, type, size, uuid)
                    .then(function (file) {
                        resolve(file);
                    }, function () {
                        removeFileAzure(uuid, isAvatar)
                        reject("Can not save file")
                        return
                    });
            }, function (error) {
                reject("Can not save file");
                return;
            })
    });
}
function saveFileLocal(stream, fileName, uuid, isAvatar) {
    let fileDir = filesFolder;
    if (!fs.existsSync(filesFolder)) {
        fs.mkdirSync(filesFolder);
    }
    if (isAvatar) {
        fileDir += avatarFolder;
        if (!fs.existsSync(fileDir)) {
            fs.mkdirSync(fileDir);
        }
    }
    return new Promise(function (resolve, reject) {
        let writeStream = fs.createWriteStream(fileDir + "/" + fileName)
        stream.pipe(writeStream)
            .on("error", function (err) {
                log.error(err)
                reject("Can not save file")
                writeStream.close()

            })

        writeStream
            .on('finish', function () {
                resolve({uuid: uuid});
                console.log("file created uuid: "+uuid);
            });
    });
}

function getFileDirectory(isAvatar){
    let fileDir = filesFolder;
    if (!fs.existsSync(filesFolder)) {
        fs.mkdirSync(filesFolder);
    }
    if (isAvatar) {
        fileDir += avatarFolder;
        if (!fs.existsSync(fileDir)) {
            fs.mkdirSync(fileDir);
        }
    }
    return fileDir;
}
function deleteFileLocal(uuid,isAvatar){
    fs.unlink(getFileDirectory(isAvatar)+"/"+uuid, function(){
        console.log("file deleted uuid: "+uuid);
    })
}

module.exports = {
    saveFile: saveFile,
    deleteFile: deleteFile,
    getFile: getFile,
    saveFileLocal: saveFileLocal,
    deleteFileLocal: deleteFileLocal,
    newFile: newFile

};

