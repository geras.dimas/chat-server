let jwt = require('jsonwebtoken');
let uuid = require("uuid");
let log = require(__base + "src/log");
let config = require(__base + "src/config");
let UserModel = require(__base + "src/data/models/user").UserModel;
let UsedRefreshTokenModel = require(__base + "src/data/models/usedRefreshToken");
const FirebaseService = require(__base + "src/services/firebaseService");

let authConfig = config.get("auth");

module.exports = {
  loginByFirebaseToken: loginByFirebaseToken,
  refreshToken: refreshToken,
  checkTokenAndUser: checkTokenAndUser,
  checkToken: checkToken,
  test: test
};

function createToken(uid) {
  return FirebaseService.generateToken(uid)
    .then(function (token) {
      return token;
    })
}

function test(tel) {
  return getOrCreateUserModel(tel)
    .then(function (userModel) {
      return {
        token: generateToken(userModel.id),
        refreshToken: generateRefreshToken(userModel.id)
      };
    });
}

function loginByFirebaseToken(token) {
  return FirebaseService.checkToken(token)
    .then(function (decodedToken) {
      let tel = decodedToken.phone_number;
      if (!tel) {
        log.debug("Firebase token is not valid - no telephone provided", decodedToken);
        return Promise.reject("Firebase token is not valid - no telephone provided");
      }
      return getOrCreateUserModel(tel)
        .then(function (userModel) {
          return {
            token: generateToken(userModel.id),
            refreshToken: generateRefreshToken(userModel.id)
          };
        });
    }, function (err) {
      log.debug("Firebase token is not valid", err);
      return Promise.reject("Firebase token is not valid");
    });
}

/**
 * Refresh token
 * @param {string} refreshToken
 * @returns {Promise}
 */
function refreshToken(refreshToken) {
  return new Promise(function (resolve, reject) {
    jwt.verify(refreshToken, authConfig.refreshTokenSecret, function (err, decoded) {
      if (err || !decoded.usr || !decoded.tid) {
        reject("Token is invalid or expired");
        return;
      }

      UsedRefreshTokenModel.findById(decoded.tid)
        .then(function (record) {
          if (record) {
            reject("This refresh token was already used");
            return;
          }

          let usedToken = new UsedRefreshTokenModel({
            _id: decoded.tid,
            createdAt: new Date(decoded.iat * 1000) // because .iat is in seconds
          });
          usedToken.save()
            .then(function () {
              resolve({
                token: generateToken(decoded.usr),
                refreshToken: generateRefreshToken(decoded.usr)
              });
            }, function (err) {
              log.error("Can't save UsedRefreshTokenModel to database", err);
              reject("Can't save UsedRefreshTokenModel to database");
            });

        }, function (err) {
          log.error("Can't get UsedRefreshTokenModel from database", refreshToken, err);
          reject("Can't get UsedRefreshTokenModel from database");
        });
    });
  });
}

/**
 * Check token and user
 * @param {string} token token
 * @returns {Promise}
 */
function checkTokenAndUser(token) {
  return new Promise(function (resolve, reject) {
    jwt.verify(token, authConfig.tokenSecret, function (err, decoded) {
      if (err || !decoded.usr) {
        reject("Token is invalid or expired");
        return;
      }

      UserModel.findById(decoded.usr)
        .then(function (record) {
          if (!record) {
            reject("User is not exists");
            return;
          }
          resolve(true);
        }, function (err) {
          log.error(err);
          reject("Can't get UserModel from database");
        });
    });
  });
}

/**
 * Check token (the main one. Not the refresh token)
 * @param {string} token token
 * @returns {boolean} true if valid
 */
function checkToken(token) {
  try {
    jwt.verify(token, authConfig.tokenSecret);
    return true;
  } catch (err) {
    return false;
  }
}

function getOrCreateUserModel(tel) {
  return UserModel.findOne({'tel': tel}, 'id tel')
    .then(function (userModel) {
      if (userModel) {
        return userModel;
      }
      return createNewUser(tel)
        .then(function (userModel) {
          return userModel;
        });
    }, function (err) {
      log.error(err);
      return Promise.reject("Can't get UserModel from database");
    });
}

function createNewUser(tel) {
  let newUser = new UserModel({
    tel: tel
  });
  return newUser.save()
    .then(function () {
      log.debug("UserModel created", newUser.id);
      subscribeService.publishMessage(newUserChannel, JSON.stringify({"userId": newUser.id}))
      return newUser;
    }, function (err) {
      log.error("Unable to create UserModel", err);
      return Promise.reject("Unable to create UserModel");
    });
}

/**
 * Generate auth token
 * @param {string} userId user UUID
 * @returns {string} token
 */
function generateToken(userId) {
  return jwt.sign({
    usr: userId  // user id
  }, authConfig.tokenSecret, {expiresIn: authConfig.tokenExpires});
}

/**
 * Generate refresh auth token (one time token)
 * @param {string} userId user UUID
 * @returns {string} refresh token
 */

function generateRefreshToken(userId) {
  return jwt.sign({
    usr: userId,   // user id
    tid: uuid.v4() // refresh token id
  }, authConfig.refreshTokenSecret, {expiresIn: authConfig.refreshTokenExpires});
}
