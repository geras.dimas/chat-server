let UserModel = require(__base + "src/data/models/user").UserModel;

module.exports = {

  /**
   * Get user by ID
   * @param {string} userId
   * @returns {Promise}
   */
  getUser: function (userId) {
    return UserModel.findById(userId)
        .populate([
            {
                path: "avatar",
                select: "_id fileName size type"
            }]).exec();
  },

  /**
   * Get list of users by ID
   * @param {Array<string>} userIds
   * @returns {Promise}
   */
  getUsers: function (userIds) {
    return UserModel.find({_id: {$in: userIds}}).exec();
  },

  /**
   * Update user
   * @param {Object} newUser (JSON with updated fields)
   * @param {string} userId
   * @returns {Promise}
   */
  updateUser: function (newUser, userId) {
    // a little security in case of "mongo injection"
    delete newUser._id;
    delete newUser.id;

    UserModel.update({_id: userId}, newUser).exec();
    return UserModel.findById(userId).exec();
  }

};
