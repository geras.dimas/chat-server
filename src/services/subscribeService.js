let redis = require("redis");
let config = require(__base + "/src/config.js");
let log = require(__base + "src/log");
let chatService = require(__base + "src/services/chatService.js");


let sub = redis.createClient({host: config.get("redis:host"), port: 6379});
let pub = redis.createClient({host: config.get("redis:host"), port: 6379});
let newMessage = config.get("redisChannel:newMessage");
let updateChat = config.get("redisChannel:updateChat");
let newUser = config.get("redisChannel:newUser");
let updateUser = config.get("redisChannel:updateUser");
let newChat = config.get("redisChannel:newChat");
let wsServer;
let listeners = {};
let chats = {};

function addListener(id_socket, id_user) {
  chatService.getUserChatsInfo(id_user)
    .then(function (userChats) {
      let listener = {id_socket: id_socket, chats: []};
      userChats.forEach(function (chat, index, array) {
        listener.chats.push(chat._id);
        chats[chat._id] = chat.members;
      });
      listeners[id_user] = listener;
      log.debug(`Add listener. Socket_id: ${id_socket}, user_id: ${id_user}`);
    });
}

function deleteListener(id_socket, id_user) {
  let listener = listeners[id_user];
  if (listener) {
    listener.chats.forEach(function (chat, index, array) {
      if (chats[chat]) {
        let membersChat = 0;
        chats[chat].forEach(function (member, index, array) {
          if (listeners[member] && member !== id_user) {
            membersChat++;
          }
        });
        if (membersChat === 0) {
          delete chats[chat];
        }
      }
    });
    delete listeners[id_user];
    log.debug(`Delete listener. Socket_id: ${id_socket}, user_id: ${id_user}`);
  }
}

function publishMessage(chanel, message) {
  pub.publish(chanel, JSON.stringify(message));
  log.debug(`Send message - ${message} in channel - ${chanel}`);
}

function notifyChatMembers(message, channel) {
  let chatNotify = chats[message.chatId];

  if (chatNotify) {
    /* socket.io room example
    if (channel === newMessage) {
      wsServer.to(message.chatId).emit('newMessageInChat', message)
    }
    if (channel === updateChat) {
      wsServer.to(message.chatId).emit('updateChat', message);
    }
    */

    chatNotify.forEach(function (member, index, array) {
      if (listeners[member]) {
        if (channel === newMessage) {
          wsServer.to(listeners[member].id_socket).emit('newMessageInChat', message)
        }
        if (channel === updateChat) {
          wsServer.to(listeners[member].id_socket).emit('updateChat', message);
        }
      }
    });
    if (channel === updateChat) {
      chatService.getChatById(message.chatId).then(function (chat) {
        chats[message.chatId] = chat.members;
      })
    }
  }
}

function notifyAllListeners(message, channel) {
  for (let user_id in listeners) {
    if (channel === newChat) {
      wsServer.to(listeners[user_id].id_socket).emit('newChatCreate', message);
    }
    if (channel === newUser) {
      wsServer.to(listeners[user_id].id_socket).emit('newUser', message);
    }
  }
}

function notifyUserListener(message, channel) {
  let userListeners = [];
  chatService.getUserChatsInfo(message.userId).then(function (userChats) {
    userChats.forEach(function (chat, index, array) {
      chat.members.forEach(function (member, index, array) {
        if (listeners[member]) {
          if (!userListeners.includes(listeners[member].id_socket))
            userListeners.push(listeners[member].id_socket);
        }
      })
    });
    userListeners.forEach(function (userListener, index, array) {
      wsServer.to(userListener).emit('userUpdate', message);
    });
  });


}

sub.on("message", function (channel, message) {
  message = JSON.parse(message);
  if (channel === newMessage || channel === updateChat) {
    notifyChatMembers(message, channel);
  }
  if (channel === newUser || channel === newChat) {
    notifyAllListeners(message, channel);
  }
  if (channel === updateUser) {
    notifyUserListener(message, channel);
  }
});

function init(serverWS) {
  sub.subscribe(newMessage);
  sub.subscribe(updateChat);
  sub.subscribe(newUser);
  sub.subscribe(updateUser);
  sub.subscribe(newChat);
  wsServer = serverWS;
  log.info("Redis subscribes created");
}

module.exports = {
  init: init,
  addListener: addListener,
  deleteListener: deleteListener,
  publishMessage: publishMessage
};
