let admin = require("firebase-admin");
let config = require(__base + "src/config.js");

class FirebaseService {
  constructor() {
    admin.initializeApp({
      credential: admin.credential.cert(config.get("firebase:sert")),
      databaseURL: config.get("firebase:databaseUrl")
    });
  }

  /**
   * Check firebase token
   * @param {string} token firebase token
   * @returns {Promise<admin.auth.DecodedIdToken>}
   */
  checkToken(token) {
    return admin.auth().verifyIdToken(token);

  }
}

module.exports = new FirebaseService();
