let config = require(__base + "src/config");
let UserModel = require(__base + "src/data/models/user").UserModel;
let ChatModel = require(__base + "src/data/models/chat").ChatModel;
let ChatMessage = require(__base + "src/data/models/chatMessage").ChatMessageModel;
let messagesChunkSize = config.get("chats:messagesChunkSize");

module.exports = {

    /**
     * Get list of chats of user
     * @param {string} userId
     * @returns {Promise}
     */
    getUserChats: function (userId) {
        return ChatModel
            .find({members: userId})
            .populate([
                {
                    path: "owner",
                    select: "_id name avatar tel"
                },
                {
                    path: "members",
                    select: "_id name avatar tel",
                    populate: {
                        path: "avatar",
                        select: "_id fileName type size"
                    }
                }
            ])
            .then(function (chats) {
                if (chats) {
                    return ChatMessage
                        .aggregate([

                            {"$match": {"chatId": {"$in": chats.map((chat) => chat._id)}}},
                            {"$sort": {"chatId": 1, "created_at": -1}},
                            {
                                "$group": {
                                    "_id": "$_id",
                                    "message": {"$first": "$message"},
                                    "author": {"$first": "$author"},
                                    "chatId": {"$first": "$chatId"},
                                    "createdAt": {"$first": "$createdAt"},
                                }
                            }
                        ])
                        .then(function (messages) {
                            return chats.map((chat) => {
                                chat._doc.lastMessage = messages.find(m => m.chatId === chat._id);
                                return chat;
                            });
                        });
                }
                return chats;
            });
    },

    getChatMessages: function (userId, chatId, from) {
        return ChatModel
            .findById(chatId)
            .exec()
            .then(function (chat) {
                if (!chat) {
                    return Promise.reject("Chat not found");
                }
                if (!chat.members || !chat.members.includes(userId)) {
                    return Promise.reject("Security error");
                }
                let qParams = {
                    chatId: chatId
                };
                if (from) {
                    qParams.created_at = {"$lt": from};
                }
                return ChatMessage
                    .find(qParams)
                    .populate([
                        {
                            path: "author",
                            select: "_id name car avatar point tel"
                        },
                        {
                            path: "file",
                            select: "_id fileName type size"
                        }
                    ])
                    .sort({'date': -1})
                    .limit(messagesChunkSize)
                    .exec();
            });
    },

    newChatMessage: function (userId, chatId, messageText, type, fileId, uuid) {
        return ChatModel
            .findById(chatId)
            .exec()
            .then(function (chat) {
                if (!chat) {
                    return Promise.reject("Chat not found");
                }
                if (!chat.members || !chat.members.includes(userId)) {
                    return Promise.reject("Security error");
                }
                let message ={message: messageText, chatId: chatId, author: userId, type: type}

                if (uuid) {
                    message._id=uuid;
                }
                if(type===2){
                    message.file=fileId;
                }
                return ChatMessage
                    .create(message)
            });
    },

    newChat: function (userId, header, type) {
        let user = UserModel.findById(userId).exec();
        return ChatModel
            .create({header: header, owner: userId, type: type, avatar: user.avatar, members: [userId]})
    },

    editChat: function (userId, chatId, header, type, avatarId, members) {
        let user = UserModel.findById(userId).exec();
        if (!avatarId) {
            avatarId = user.avatar;
        }
        ChatModel.update({_id: chatId}, {
            header: header,
            avatar: avatarId,
            type: type,
            members: members
        }).exec();
        return ChatModel.findById(chatId).exec()
    },

    editChatMembers: function (chatId, members) {
        ChatModel.update({_id: chatId}, {
            members: members
        }).exec();
        return ChatModel.findById(chatId).exec()
    },

    getChatById: function (chatId) {
        return ChatModel.findById(chatId).exec()
    },

    getUserChatsInfo: function (userId) {
        return ChatModel.find({members: userId}, '_id members').exec()
    }
};
