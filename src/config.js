let nconf = require("nconf");

let envConfigFile = process.env.NODE_ENV === "production" ? "./config.prod.json" : "./config.dev.json";

nconf.argv()
    .env()
    .file({file: "./config.json"}) // main (default) config
    .file({file: envConfigFile}); // prod / dev config

console.log("NODE_ENV: " + process.env.NODE_ENV);
console.log("Selected config: " + envConfigFile);

module.exports = nconf;