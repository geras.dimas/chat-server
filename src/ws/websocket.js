let socketioJwt = require('socketio-jwt');
let log = require(__base + "src/log");
let config = require(__base + "src/config");
let userService = require(__base + "src/services/userService.js");
let chatService = require(__base + "src/services/chatService.js");
let fileService = require(__base + "src/services/fileService.js");
let subscribeService = require(__base + "src/services/subscribeService.js");
let connections = {};
let newMessage = config.get("redisChannel:newMessage");
let updateChat = config.get("redisChannel:updateChat");
let newUser = config.get("redisChannel:newUser");
let updateUserChannel = config.get("redisChannel:updateUser");
let newChat = config.get("redisChannel:newChat");
let fs = require('fs');
let ss = require('socket.io-stream');
let Readable = require('stream').Readable;
let mimeType = require('mime-types');


function onAuthenticate(socket) {
    let userId = socket.decoded_token.usr;
    connections[userId] = socket;
    subscribeService.addListener(socket.id, userId);
    log.debug("WebSocket connected: " + userId);

    /*
    {
      id: 123,
    }
     */
    socket.on('getUser', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        userService.getUser(userId)
            .then(function (user) {
                if (!user) {
                    socket.emit("getUser", {
                        id: requestBody.id,
                        status: 2,
                        error: "Not found"
                    });
                    return;
                }
                socket.emit("getUser", {
                    id: requestBody.id,
                    status: 0,
                    data: user
                });
            }, function () {
                socket.emit("getUser", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
            });
    });

    socket.on('getUserById', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.userId) {
            socket.emit("getUserById", {
                id: requestBody.id,
                status: 3,
                error: "Bad request"
            });
            return;
        }
        userService.getUser(requestBody.data.userId)
            .then(function (user) {
                if (!user) {
                    socket.emit("getUserById", {
                        id: requestBody.id,
                        status: 2,
                        error: "Not found"
                    });
                    return;
                }
                socket.emit("getUserById", {
                    id: requestBody.id,
                    status: 0,
                    data: user
                });
            }, function () {
                socket.emit("getUserById", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
            });
    });

    /*
    {
      id: 123,
      data: {
          lat: 12341234,
          lng: 234123423
      }
    }
     */


    socket.on('updateUser', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data) {
            socket.emit("updateUser", {
                id: requestBody.id,
                status: 3,
                error: "Bad request"
            });
            return;
        }
        userService.updateUser(requestBody.data, userId)
            .then(function (user) {
                if (!user) {
                    socket.emit("updateUser", {
                        id: requestBody.id,
                        status: 2,
                        error: "Not found"
                    });
                    return;
                }
                subscribeService.publishMessage(updateUserChannel, {userId: user._id});
                socket.emit("updateUser", {
                    id: requestBody.id,
                    status: 0,
                    data: user
                });
            }, function () {
                socket.emit("updateUser", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
            });
    });

    /*
    {
      id: 123,
    }
     */
    socket.on('getUserChats', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        chatService.getUserChats(userId)
            .then(function (chats) {
                socket.emit("getUserChats", {
                    id: requestBody.id,
                    status: 0,
                    data: chats
                });
            }, function () {
                socket.emit("getUserChats", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
            });
    });

    /*
    {
      id: 123,
      data: {
          chatId: c92n9
      }
    }
     */
    socket.on('getChatMessages', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.chatId) {
            socket.emit("getChatMessages", {
                id: requestBody.id,
                status: 3,
                error: "Bad request"
            });
            return;
        }

        let chatId = requestBody.data.chatId;
        let from = requestBody.data.from;
        chatService.getChatMessages(userId, chatId, from)
            .then(function (messages) {
                socket.emit("getChatMessages", {
                    id: requestBody.id,
                    status: 0,
                    data: messages
                });
            }, function () {
                socket.emit("getChatMessages", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
            });
    });

    socket.on('newMessage', function (requestBody) {
            if (!requestBody.id) {
                return;
            }
            if (!requestBody.data || !requestBody.data.chatId || !requestBody.data.message || !requestBody.data.type) {
                socket.emit("newMessage", {
                    id: requestBody.id,
                    status: 3,
                    error: "Bad request"
                });
                return;
            }
            if (requestBody.data.message.length > 8192) {
                socket.emit("newMessage", {
                    id: requestBody.id,
                    status: 3,
                    error: "Message too long"
                });
                return;
            }
            let fileId = null;
            let chatId = requestBody.data.chatId;
            let message = requestBody.data.message;
            let type = requestBody.data.type;
            let uuid = requestBody.data.id;
            if (requestBody.data.type == 2) {
                if (!requestBody.data.file) {
                    socket.emit("newMessage", {
                        id: requestBody.id,
                        status: 3,
                        error: "Bad request"
                    });
                    return;
                }

                if (!requestBody.data.file.fileName || !requestBody.data.file.type || !requestBody.data.file.size || !requestBody.data.file.id) {

                    socket.emit("newMessage", {
                        id: requestBody.id,
                        status: 3,
                        error: "Bad request"
                    });
                    return;
                }
                fileId = requestBody.data.file.id;
                fileService.newFile(requestBody.data.file.fileName, requestBody.data.file.type, requestBody.data.file.size, requestBody.data.file.id)
                    .then(function (file) {
                        chatService.newChatMessage(userId, chatId, message, type, fileId, uuid)
                            .then(function (message) {
                                subscribeService.publishMessage(newMessage, {
                                    "chatId": chatId,
                                    message: message.message
                                });
                                socket.emit("newMessage", {
                                    id: requestBody.id,
                                    status: 0,
                                    data: message
                                });
                            }, function (error) {
                                socket.emit("newMessage", {
                                    id: requestBody.id,
                                    status: 1,
                                    error: "Internal Server Error"
                                });
                            });
                    }, function (error) {
                        socket.emit("newMessage", {
                            id: requestBody.id,
                            status: 1,
                            error: "Internal Server Error"
                        });
                    })

            }
            else {

                chatService.newChatMessage(userId, chatId, message, type, fileId, uuid)
                    .then(function (message) {
                        subscribeService.publishMessage(newMessage, {"chatId": chatId, message: message.message});
                        socket.emit("newMessage", {
                            id: requestBody.id,
                            status: 0,
                            data: message
                        });
                    }, function (error) {
                        socket.emit("newMessage", {
                            id: requestBody.id,
                            status: 1,
                            error: "Internal Server Error"
                        });
                    });

            }
        }
    )
    ;
    /*
    {
      id: 123,
      data: {
          header: "message text",
          type: "0", // 0/1
          lat: 12341234, // опционально
          lng: 234123423 // опционально
      }
    }
     */
    socket.on('newChat', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.header || !requestBody.data.type === undefined) {
            socket.emit("newChat", {
                id: requestBody.id,
                status: 3,
                error: "Bad request"
            });
            return;
        }
        let header = requestBody.data.header;
        let type = requestBody.data.type;


        chatService.newChat(userId, header, type)
            .then(function (chat) {
                socket.emit("newChat", {
                    id: requestBody.id,
                    status: 0,
                    data: chat
                });
            }, function () {
                socket.emit("newChat", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            })

    });
    /*
     {
      id: 123,
      data: {
          header: "message text",
          type: "0", // 0/1
          lat: 12341234,
          lng: 234123423
          avatar: "guid"
      }
    }
    */

    socket.on('joinChat', function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.chatId) {
            socket.emit("editChat", {
                id: requestBody.id,
                status: 3,
                error: "Bad request."
            });
        }
        chatService.getChatById(requestBody.data.chatId)
            .then(function (chat) {
                if (!chat) {
                    socket.emit("joinChat", {
                        id: requestBody.id,
                        status: 2,
                        error: "Not found"
                    });
                    return;
                }
                let members = chat.members;
                if (members.indexOf(userId) == -1) {
                    members.push(userId);
                } else {
                    return;
                }
                chatService.editChatMembers(requestBody.data.chatId, members)
                    .then(function (chat2) {
                        subscribeService.publishMessage(updateChat, {chatId: chat2._id});
                        socket.emit("joinChat", {
                            id: requestBody.id,
                            status: 0,
                            data: chat2
                        });
                    }, function () {
                        socket.emit("joinChat", {
                            id: requestBody.id,
                            status: 1,
                            error: "Internal Server Error"
                        });
                        return;
                    })

            }, function () {
                socket.emit("joinChat", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            })
    })

    socket.on("uploadAvatar", function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data.name || !requestBody.data.uuid || !requestBody.data.size) {
            socket.emit("uploadAvatar", {
                id: requestBody.id,
                status: 3,
                error: "Bad request."
            });
            return;
        }
        /*if (requestBody.data.size > avatarMaxSize) {
          socket.emit("downloadAvatar", {
            id: requestBody.id,
            status: 3,
            error: "The file is larger than the allowed size"
          });
          return;
        }*/

        let ext = mimeType.lookup(requestBody.data.name);
        if (!ext.includes("image/")) {
            socket.emit("uploadAvatar", {
                id: requestBody.id,
                status: 3,
                error: "Bad file extension"
            });
            return;
        }
        let stream = new Readable();
        stream._read = function () {
        };
        socket.on("uploadAvatar-data", function (subrequestBody) {
            if (!subrequestBody.id) {
                return;
            }
            if (subrequestBody.id != requestBody.id) {
                return;
            }
            if (!subrequestBody.data.chunk) {
                return;
            }

            stream.push(subrequestBody.data.chunk)

        });

        socket.on("uploadAvatar-end", function (subrequestBody) {
            if (!subrequestBody.id) {
                return;
            }
            if (subrequestBody.id != requestBody.id) {
                return;
            }
            stream.push(null);

        });

        fileService.saveFileLocal(stream, requestBody.data.uuid, requestBody.data.uuid, true)
            .then(function (uuid) {

                socket.removeAllListeners("uploadAvatar-end");
                socket.removeAllListeners("uploadAvatar-data");
                fileService.saveFile(requestBody.data.size, requestBody.data.name, ext, requestBody.data.uuid, true)
                    .then(function (avatar) {
                        socket.emit("uploadAvatar", {
                            id: requestBody.id,
                            status: 0,
                            data: avatar
                        });
                        fileService.deleteFileLocal(requestBody.data.uuid, true);
                        return;
                    }, function (error) {
                        socket.emit("uploadAvatar", {
                            id: requestBody.id,
                            status: 1,
                            error: "Internal Server Error"
                        });
                        fileService.deleteFileLocal(requestBody.data.uuid, true);
                        return;
                    });
            }, function (error) {
                socket.emit("uploadAvatar", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            });
        /*fileService.saveFile(requestBody.data.dataArray, requestBody.data.name, ext, requestBody.data.uuid, true)
            .then(function (avatar) {
                socket.emit("uploadAvatar", {
                    id: requestBody.id,
                    status: 0,
                    data: avatar
                });
                return;
            }, function () {
                socket.emit("uploadAvatar", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            });
            */
    });

    socket.on("uploadFile", function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data.name || !requestBody.data.uuid || !requestBody.data.size) {
            socket.emit("uploadFile", {
                id: requestBody.id,
                status: 3,
                error: "Bad request."
            });
            return;
        }
        let ext = mimeType.lookup(requestBody.data.name);
        let stream = new Readable();
        stream._read = function () {
        };
        socket.on("uploadFile-data", function (subrequestBody) {
            if (!subrequestBody.id) {
                return;
            }
            if (subrequestBody.id != requestBody.id) {
                return;
            }
            if (!subrequestBody.data.chunk) {
                return;
            }

            stream.push(subrequestBody.data.chunk)

        });

        socket.on("uploadFile-end", function (subrequestBody) {
            if (!subrequestBody.id) {
                return;
            }
            if (subrequestBody.id != requestBody.id) {
                return;
            }
            stream.push(null);

        });
        fileService.saveFileLocal(stream, requestBody.data.uuid, requestBody.data.uuid, false)
            .then(function (uuid) {
                socket.removeAllListeners("uploadFile-end");
                socket.removeAllListeners("uploadFile-data");
                fileService.saveFile(requestBody.data.size, requestBody.data.name, ext, requestBody.data.uuid, false)
                    .then(function (file) {
                        socket.emit("uploadFile", {
                            id: requestBody.id,
                            status: 0,
                            data: file
                        });
                        fileService.deleteFileLocal(requestBody.data.uuid, false);
                        return;
                    }, function (error) {
                        socket.emit("uploadFile", {
                            id: requestBody.id,
                            status: 1,
                            error: "Internal Server Error"
                        });
                        fileService.deleteFileLocal(requestBody.data.uuid, false);
                        return;
                    });
            }, function (error) {
                socket.emit("uploadFile", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            });
    });

    socket.on("getFile", function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.fileId) {
            socket.emit("getFile", {
                id: requestBody.id,
                status: 3,
                error: "Bad request."
            });
            return;
        }
        fileService.getFile(requestBody.data.fileId)
            .then(function (file) {
                if (!file) {
                    socket.emit("getFile", {
                        id: requestBody.id,
                        status: 2,
                        error: "Not found"
                    });
                    return;
                }
                socket.emit("getFile", {
                    id: requestBody.id,
                    status: 0,
                    data: file
                });
                return;
            }, function () {
                socket.emit("getFile", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            })
    });

    socket.on("deleteFile", function (requestBody) {
        if (!requestBody.id) {
            return;
        }
        if (!requestBody.data || !requestBody.data.fileId || requestBody.data.isAvatar == undefined) {
            socket.emit("deleteFile", {
                id: requestBody.id,
                status: 3,
                error: "Bad request."
            });
            return;
        }
        fileService.deleteFile(requestBody.data.fileId, requestBody.data.isAvatar)
            .then(function (res) {

                socket.emit("deleteFile", {
                    id: requestBody.id,
                    status: 0,
                    data: res
                });
                return;
            }, function (error) {
                socket.emit("deleteFile", {
                    id: requestBody.id,
                    status: 1,
                    error: "Internal Server Error"
                });
                return;
            })
    });
    socket.on('disconnect', function () {
        log.debug("WebSocket disconnected: " + userId);
        subscribeService.deleteListener(socket.id, userId);
        connections[userId] = void 0;

        // todo disable subscriptions
    });

    socket.on('error', function (err) {
        log.error("WebSocket Error: " + userId, err);
    });
}

function sendStartInform(socket) {
    let userId = socket.decoded_token.usr;
    userService.getUser(userId)
        .then(function (user) {
            if (!user) {
                socket.emit("getUser", {
                    status: 2,
                    error: "Not found"
                });
                return;
            }
            socket.emit("getUser", {
                status: 0,
                data: user
            });
        }, function () {
            socket.emit("getUser", {
                status: 1,
                error: "Internal Server Error"
            });
        });
    chatService.getUserChats(userId)
        .then(function (chats) {
            socket.emit("getUserChats", {
                status: 0,
                data: chats
            });
        }, function () {
            socket.emit("getUserChats", {
                status: 1,
                error: "Internal Server Error"
            });
        });
    fs.readFile('./config-apk.json', 'utf8', function (err, data) {
        if (err) {
            socket.emit("getApkConfig", {
                status: 0,
                data: {
                    pointGetTimeInterval: config.get("defaultApkConfig:pointGetTimeInterval"),
                    pointGetInterval: config.get("defaultApkConfig:pointGetInterval"),
                    Storage: config.get("files:azure:azureStorage"),
                    AvatarStorageContainer: config.get("files:azure:azureAvatarStorageContainer"),
                    StorageContainer: config.get("files:azure:azureStorageContainer"),
                    maxSizeAvatar: 5242880,
                    maxSizeFile: 10485760
                }
            });
            return;
        }
        let apkConfig = JSON.parse(data);
        socket.once
        socket.emit("getApkConfig", {
            status: 0,
            data: apkConfig
        })
    });
}

function init(wsServer) {
    wsServer
        .on('connection', socketioJwt.authorize({
            secret: config.get("auth:tokenSecret"),
            timeout: config.get("auth:webSocketAuthDelay") // 15 seconds to send the authentication message
        }))
        .on('authenticated', function (socket) {
            // this socket is authenticated, we are good to handle more events from it.
            sendStartInform(socket);
            onAuthenticate(socket);

        })
        .on('connect', function (socket) {
            // this socket is authenticated, we are good to handle more events from it.
        });
}

module.exports = {
    init: init
};
